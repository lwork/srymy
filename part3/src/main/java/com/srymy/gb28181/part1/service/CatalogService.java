package com.srymy.gb28181.part1.service;

import com.srymy.gb28181.part1.sip.SipServerLayer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.InvalidArgumentException;
import javax.sip.SipException;
import java.text.ParseException;

/**
 * @Author: cth
 * @Date: 2020/9/16 16:04
 * @Description:
 */
@Service
public class CatalogService {
    @Autowired
    SipServerLayer sipLayer;
    public void sendCatalog() throws ParseException, SipException, InvalidArgumentException {
        sipLayer.sendCatalog("192.168.1.200",5060,"34020000001110000001");
    }
}
