package com.srymy.gb28181.part1.config;

import com.srymy.gb28181.part1.sip.SipSrymyMessageProcessor;
import com.srymy.gb28181.part1.sip.SipServerLayer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sip.PeerUnavailableException;

/**
 * @Author 授人以鳗鱼
 * @Date 9/25/2019
 */
@Slf4j
@Configuration
public class SipSrymyConfig {
    @Value("${gb28181.deviceId:34020000002000000001}")
    String deviceId;
    @Value("${gb28181.ip:172.18.99.169}")
    String ip;
    @Value("${gb28181.port:5060}")
    Integer port;

    @Bean
    public SipServerLayer sipLayer(SipSrymyMessageProcessor messageProcessor) {
        try {
            SipServerLayer sipServerLayer = new SipServerLayer(ip, port);
            sipServerLayer.setMessageProcessor(messageProcessor);
            log.info("SIP服务启动完毕, 已经在[{}:{}]端口监听SIP国标消息", ip, port);
            return sipServerLayer;
        } catch (PeerUnavailableException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}


